<?php 
	session_start();

	$itemId = $_GET['name'];

	unset($_SESSION['cart'][$itemId]);

	header("Location: ". $_SERVER['HTTP_REFERER']);

?>