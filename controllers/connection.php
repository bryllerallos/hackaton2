<?php 
	$host = "localhost"; //host that will be used;
	$db_username = "root"; //username for the host;
	$db_password = ""; //password for the host;
	$db_name = "b55ecommerce"; //name of the database;

	// to create the connection in database
	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);
	//to check the connection
	if(!$conn) {
		die("Connection failed: ". mysqli_error($conn));
	}

?>