<!DOCTYPE html>
<html>
<head>
	<title><?php get_title(); ?></title>
	<!-- bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">MSX</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarColor02">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="../views/catalog.php">Products <span class="sr-only">(current)</span></a>
					</li>

					<?php 
						session_start();
						if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] ==1){
					?>
						<li class="nav-item">
						<a class="nav-link" href="add-item.php">Add Item</a>
					</li>


					<?php		
						}else{

					?>
					<li class="nav-item">
						<a class="nav-link" href="cart.php">My Cart <span class="badge bg-danger" id="cartCount"><?php 
							
							if(isset($_SESSION['cart'])){
								echo array_sum($_SESSION['cart']);

							}else{
								echo 0;
							}
						

						 ?></span></a>
					</li>
					

					<?php		
						}

						if(isset($_SESSION['user']['firstName'])){
					?>	
					<li class="nav-item">
						<a class="nav-link" href="profile.php">Hello <?php echo $_SESSION['user']['firstName'] ?> !</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="order-history.php">Transaction History</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="../controllers/logout-process.php">Logout</a>
					</li>


					<?php
						}else {
					?>	
						<li class="nav-item">
							<a class="nav-link" href="login.php">Login</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="register.php">Register</a>
					</li>

					<?php
						}

					 ?>		
					
				</ul>
			</div>
		</nav>
	</header>

	<?php get_body_contents() ?>

	<footer class="page-footer font-small bg-info navbar-dark">
		<div class="footer-copyright text-center text-light py-3">
			MSX 2020® All rights reserved!
		</div>

	</footer>
	<!-- bootswatch -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>