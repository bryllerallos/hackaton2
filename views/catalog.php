<?php 
require "../partials/template.php";



function get_title(){
	echo "Catalog";
}

function get_body_contents(){
		//require connection from database
	require "../controllers/connection.php";

	

	?>

	<h1 class="text-center py-5">Top Picks</h1>
	<div class="container">
		<!-- item list -->
		<div class="row align-items-center justify-content-center">
			<?php 
			//publish items
			$items_query = "SELECT * FROM items";
			$items = mysqli_query($conn, $items_query);
			// var_dump($items);
			// die();
			foreach ($items as $indiv_item){
			// var_dump($indiv_item);
			// die();
				?>
				<div class="col-lg-4 py-2">
					<div class="card">
						<img  class="card-img-top" height="200px" style="object-fit: contain;" src="<?php echo $indiv_item['imgPath'] ?>">
						<div class="card-body text-center">
							<h4 class=" card-title"><?= $indiv_item['name']?></h4>
						</div>
						<div class="card-body text-center">
							<p class="card-text">Price: <?= $indiv_item['price']?></p>
							<p class="card-text">Description: <?= $indiv_item['description']?></p>
							<p class="card-text">Category: 
								<?php 
								$catId = $indiv_item['category_id'];

								$category_query = "SELECT * FROM categories WHERE id = $catId";
								$category = mysqli_fetch_assoc(mysqli_query($conn, $category_query));
							// var_dump($category);

								echo $category['name'];

								?>
							</p>
						</div>
						<div class="card-footer text-center">
							<?php if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] ==1){ ?>
							<a class="btn btn-danger" href="../controllers/delete-item-process.php?id=<?php echo $indiv_item['id']?>">Delete Item</a>
							<a  class="btn btn-warning" href="edit-item.php?id=<?php echo $indiv_item['id']?>">Edit Item</a><?php } ?>
						</div>
						<div class="card-footer text-center">
							<!-- <form action="../controllers/add-to-cart-process.php" method="POST"> -->
								<!-- hidden value of item -->
							<!-- <input type="hidden" name="id" value="<?php echo $indiv_item['id']?>"> -->
							<input type="number" name="cart" class="form-control" value="1">
							<button type="button" class="btn btn-primary addToCart" data-id="<?php echo $indiv_item['id'];?>">Add to Cart</button>
							<!-- </form> -->
						</div>
					</div>
				</div>


				<?php	

			}
			?>
		</div>
	</div>
	<script type="text/javascript" src="../assets/scripts/add-to-cart.js"></script>	
	<?php
}
?>
