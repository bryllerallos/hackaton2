<?php 

	require "../partials/template.php";

	function get_title(){
		echo "Profile";
	}

	function get_body_contents(){

	require "../controllers/connection.php";	

	$firstName = $_SESSION['user']['firstName'];
	$lastName = $_SESSION['user']['lastName'];
	$email = $_SESSION['user']['email'];


?>
	<h1 class="text-center"></h1>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">	
		<h3>
			First name: <span class="text-info"><?php echo $firstName ?></span>
		</h3>
		<h3 >
			Last name: <span class="text-info"><?php echo $lastName ?></span>
		</h3>
		<h3>
			Email: <span class="text-info"><?php echo $email ?></span>
		</h3>

	<h3>Contact Numbers: </h3>
			<ul>
				<?php 
				$userId = $_SESSION['user']['id'];
				$contacts_query = "SELECT * FROM contacts WHERE user_id = $userId";
				$contacts = mysqli_query($conn, $contacts_query);
				foreach($contacts as $indiv_contacts){
					?>
					<li>
						<?php echo $indiv_contacts['contactNo'];	?>
					</li>

					<?php
				}
				 ?>

			</ul>

		<h3>Address:</h3>
			<ul>
				<?php 
				$userId = $_SESSION['user']['id'];
				$address_query = "SELECT * FROM addresses WHERE user_id = $userId";

				$addresses = mysqli_query($conn, $address_query);


				foreach($addresses as $indiv_address){

				?>
				<li><?php echo $indiv_address['address1'] .  "," . $indiv_address['address2']. "<br>" . $indiv_address['city']. "," . $indiv_address['zipcode']?>
				</li>	

				<?php	
				}
				 ?>

			</ul>

			</div>
		<div class="col-lg-6">
			<form action="../controllers/add-contact-process.php" method="POST">
					<div class="form-group">
						<label for="contacts">
							Contact Number:
						</label>
						<input type="number" name="contacts" class="form-control">
					</div>
					<div class="text-center">
					<input type="hidden" name="user_id" value="<?php echo $userId ?>">
					<button class="btn btn-success" type="submit">Add Contact Number</button>
					</div>
				</form>
			
			
			<form action="../controllers/add-address-process.php" method="POST">
				<div class="form-group">
					<label for="address1">Address 1:</label>
					<input type="text" name="address1" class="form-control">
				</div>
				<div class="form-group">
					<label for="address2">Address 2:</label>
					<input type="text" name="address2" class="form-control">
				</div>
				<div class="form-group">
					<label for="city">City:</label>
					<input type="text" name="city" class="form-control">
				</div>
				<div class="form-group">
					<label for="zipCode">Zipcode:</label>
					<input type="number" name="zipCode" class="form-control">
				</div>
				<div class="text-center my-3">
				<input type="hidden" name="userId" value="<?php echo $userId?>">
				<button class="btn btn-success" type="submit">Add Address</button>
				</div>
			</form>




		</div>




	</div>

</div>




<?php
	
	}

 ?>