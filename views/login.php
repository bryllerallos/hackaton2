<?php 

require "../partials/template.php";

function get_title(){
	echo "Login";

}
function get_body_contents(){


	?>
	<div class="container">
		<h1 class="text-center py-5">Log In</h1>
		<div class="col-lg-8 offset-lg-2">
			<form action="" method="POST">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control" id="email">
					<span class="validation text-danger"></span>
				</div>
				<div class="form-group">
					<label for="password">Password:</label>
					<input type="text" name="password" class="form-control" id="password">
					<span class="validation"></span>
				</div>
				<div class="form-group text-center">
					<button  type="button" class="btn btn-info" id="loginUser">Login</button>
					<p>No Account Yet?<a href="register.php">Register</a></p>
				</div>



			</form>

		</div>
		<script src="../assets/scripts/login.js" ></script>
	</div>



	<?php

}




?>